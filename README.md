# REPL exercise

Test-driven development exercise.
The focus lies on writing tests and running the test suite.
To add functionality, add tests that test this functionality, then add the implementation afterwards.

Everything up to built-in functions (with the exception of implied summation for simple integer list expressions) should be supported.
Only one built-in is implemented: `sum(a, b)`.

