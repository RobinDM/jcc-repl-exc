package jar;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FractionTest {

    @Test
    void simplify() {
        Fraction f = new Fraction(
                2,
                4
        );
        assertEquals(
                1,
                f.getNumerator()
        );
        assertEquals(
                2,
                f.getDenominator()
        );
    }

    @Test
    void getNumerator() {
        Fraction f = new Fraction(
                1,
                2
        );
        assertEquals(
                1,
                f.getNumerator()
        );
    }

    @Test
    void getDenominator() {
        Fraction f = new Fraction(
                1,
                2
        );
        assertEquals(
                2,
                f.getDenominator()
        );
    }

    @Test
    void testToString() {
        Fraction f = new Fraction(
                1,
                2
        );
        assertEquals(
                "1/2",
                f.toString()
        );
    }

    @Test
    void testNegativeNumerator() {
        Fraction f = new Fraction(
                -4,
                12
        );
        assertEquals(
                -1,
                f.getNumerator()
        );
        assertEquals(
                3,
                f.getDenominator()
        );
    }

    @Test
    void testNegativeDenominator() {
        Fraction f = new Fraction(
                4,
                -12
        );
        assertEquals(
                1,
                f.getNumerator()
        );
        assertEquals(
                -3,
                f.getDenominator()
        );
    }

    @Test
    void testDoubleNegative() {
        Fraction f = new Fraction(
                -4,
                -12
        );
        assertEquals(
                1,
                f.getNumerator()
        );
        assertEquals(
                3,
                f.getDenominator()
        );
    }

    @Test
    void denominatorShouldNotBeZero() {
        assertThrows(
                ArithmeticException.class,
                () -> {
                    Fraction f = new Fraction(
                            5,
                            0
                    );
                }
        );
    }

    @Test
    void testFractionAdditionMathIsRight() {
        Fraction f1 = new Fraction(
                2,
                5
        );
        Fraction f2 = new Fraction(
                3,
                7
        );
        Fraction sum = f1.add(f2);
        assertEquals(
                29,
                sum.getNumerator()
        );
        assertEquals(
                35,
                sum.getDenominator()
        );
    }

    @Test
    void testFractionAdditionIsImmutable() {
        Fraction f1 = new Fraction(
                2,
                5
        );
        Fraction f2 = new Fraction(
                3,
                10
        );
        Fraction sum = f1.add(f2);
        assertEquals(
                2,
                f1.getNumerator()
        );
        assertEquals(
                5,
                f1.getDenominator()
        );
        assertEquals(
                3,
                f2.getNumerator()
        );
        assertEquals(
                10,
                f2.getDenominator()
        );
    }

    @ParameterizedTest
    @MethodSource("provideFractionTriplesForSubtraction")
    void testFractionSubtractionParametrized(
            Fraction i1,
            Fraction i2,
            Fraction out) {
        Fraction sum = i1.subtract(i2);
        assertEquals(
                sum.getNumerator(),
                out.getNumerator()
        );
        assertEquals(
                sum.getDenominator(),
                out.getDenominator()
        );
    }

    private static Stream<Arguments> provideFractionTriplesForSubtraction() {
        return Stream.of(
                Arguments.of(
                        new Fraction(
                                1,
                                2
                        ),
                        new Fraction(
                                1,
                                3
                        ),
                        new Fraction(
                                1,
                                6
                        )
                ),
                Arguments.of(
                        new Fraction(
                                2,
                                7
                        ),
                        new Fraction(
                                4,
                                3
                        ),
                        new Fraction(
                                3 * 2 - 4 * 7,
                                3 * 7
                        )
                ),
                Arguments.of(
                        new Fraction(
                                5,
                                6
                        ),
                        new Fraction(
                                13,
                                2
                        ),
                        new Fraction(
                                2 * 5 - 6 * 13,
                                6 * 2
                        )
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideFractionTriplesForAddition")
    void testFractionAdditionParametrized(
            Fraction i1,
            Fraction i2,
            Fraction out) {
        Fraction sum = i1.add(i2);
        assertEquals(
                sum.getNumerator(),
                out.getNumerator()
        );
        assertEquals(
                sum.getDenominator(),
                out.getDenominator()
        );
    }

    private static Stream<Arguments> provideFractionTriplesForAddition() {
        return Stream.of(
                Arguments.of(
                        new Fraction(
                                1,
                                2
                        ),
                        new Fraction(
                                1,
                                3
                        ),
                        new Fraction(
                                5,
                                6
                        )
                ),
                Arguments.of(
                        new Fraction(
                                2,
                                7
                        ),
                        new Fraction(
                                4,
                                3
                        ),
                        new Fraction(
                                3 * 2 + 4 * 7,
                                3 * 7
                        )
                ),
                Arguments.of(
                        new Fraction(
                                5,
                                6
                        ),
                        new Fraction(
                                13,
                                2
                        ),
                        new Fraction(
                                2 * 5 + 6 * 13,
                                6 * 2
                        )
                )
        );
    }

    @Test
    void negativeNumeratorIsReportedAsNegative(){
        Fraction f = new Fraction(-1, 2);
        assertEquals(true, f.isNegative());
    }

    @Test
    void negativeDenominatorIsReportedAsNegative(){
        Fraction f = new Fraction(1, -2);
        assertEquals(true, f.isNegative());
    }

    @Test
    void doubleNegativeIsNotNegative(){
        Fraction f = new Fraction(-1, -2);
        assertEquals(false, f.isNegative());
    }

    @ParameterizedTest
    @MethodSource("provideNegativePositiveCombinations")
    void negativeIsOppositeOfPositive(Fraction f){
        assertEquals(f.isNegative(), !f.isPositive());
    }

    static Stream<Fraction> provideNegativePositiveCombinations() {
        return Stream.of(
                new Fraction(1, 2),
                new Fraction(-1, 2),
                new Fraction(1, -2),
                new Fraction(-1, -2)
        );
    }
}