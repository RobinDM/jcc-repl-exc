package jar;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add() {
        int sum = new Calculator().add(5, 3);
        assertEquals(8, sum);
    }

    @Test
    void addHamcrest() {
        int sum = new Calculator().add(5, 3);
        MatcherAssert.assertThat(sum, Matchers.is(8));
    }

    @Test
    void twoNumbersResultInSum(){
        assertEquals(9, new Calculator().add("4 5"));
    }

    @Test
    void oneNumberResultsInNumber(){
        assertEquals(7, new Calculator().add("7"));
    }

    @Test
    void emptyStringResultsInZero(){
        assertEquals(0, new Calculator().add(""));
    }

    @Test
    void invalidStringShouldCrash(){
        assertThrows(Exception.class, () -> {
            new Calculator().add("a 5");
        });
    }
}