package jar;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class REPLTest {

    @ParameterizedTest
    @ValueSource(strings = {"9", "7", "5"})
    void singleNumberYieldsANumber(String input) {
        assertEquals(
                Integer.parseInt(input),
                new REPL().eval(input)
        );
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"a", "7 +", "3 5", "3 +5", "+ 3", "3+5", "- 4", "3 + -", "+", "3 + - 5", "-", "*", "/", "(5 + 3", "5 + 4)", "3 + ("})
    void invalidInputThrowsException(String faultyInput) {
        assertThrows(
                Exception.class,
                () -> new REPL().eval(faultyInput)
        );
    }

    @Test
    void sumTokenComputesSum() {
        assertEquals(
                57,
                new REPL().eval("7 + 50")
        );
    }

    @Test
    void minusTokenComputesSubtraction() {
        assertEquals(
                4,
                new REPL().eval("7 - 3")
        );
    }

    @Test
    void asteriskTokenComputesMultiplication() {
        assertEquals(
                40,
                new REPL().eval("5 * 8")
        );
    }

    @Test
    void divTokenComputesDivision() {
        assertEquals(
                2,
                new REPL().eval("24 / 12")
        );
    }

    @Test
    void twoOperatorsInOneInputAreParsed() {
        assertEquals(
                99,
                new REPL().eval("37 - 12 + 74")
        );
    }

    @Test
    void parenthesisOverridePrecedence() {
        assertEquals(
                186,
                new REPL().eval("1 + (5 * (2 + (5 * 7)))")
        );
    }

    @Test
    void leftToRightPrecendence() {
        //oddly...
        assertEquals(
                21,
                new REPL().eval("2 + 5 * 3")
        );
    }

    @Test
    void acceptParentheses() {
        assertEquals(
                5,
                new REPL().eval("(2 + 3)")
        );
    }

    @Test
    void variableDeclaration() {
        REPL repl = new REPL();
        assertThrows(Exception.class, () -> repl.eval("x"));
        repl.eval("x = 7");
        assertEquals(
                7,
                repl.eval("x")
        );
    }

    @Test
    void testExceptionWhenUsingUndeclaredVariable() {
        assertThrows(Exception.class, () -> new REPL().eval("3 + x"));
    }

    @Test
    void variableInExpressionIsEvaluated() {
        REPL repl = new REPL();
        repl.eval("x = 2");
        assertEquals(
                7,
                repl.eval("x + 5")
        );
    }

    @Test
    void chainedAndNested(){
        assertEquals(
                9,
                new REPL().eval("6 - 3 + (7 - 1)")
        );
    }

    @Test
    void builtInSum() {
        assertEquals(
                5,
                new REPL().eval("sum(3, 2)")
        );
    }

    @Test
    void builtInNestedSum() {
        assertEquals(
                -1,
                new REPL().eval("2  + sum(3, 5) - sum(7, sum(3, 1))")
        );
    }

    @Test
    void sumWithVariables() {
        REPL repl = new REPL();
        repl.eval("x = 2");
        assertEquals(
                4,
                repl.eval("sum(x, x)")
        );
        assertEquals(
                4,
                repl.eval("sum(x, 2)")
        );
        assertEquals(
                4,
                repl.eval("sum(2, x)")
        );
    }
}
