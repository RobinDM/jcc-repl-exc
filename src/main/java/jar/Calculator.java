package jar;

import java.util.Arrays;

public class Calculator {
    public int add(
            int a,
            int b) {
        return a + b;
    }

    public int add(String serialized){
        if (serialized == null || serialized.trim().isEmpty()){
            return 0;
        }
        String[] parts = serialized.split(" ");
        return Arrays.stream(parts).mapToInt(s -> Integer.parseInt(s.trim())).sum();
    }
}
