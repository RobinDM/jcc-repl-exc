package jar;

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(
            int numerator,
            int denominator) {
        if(denominator == 0){
            throw new ArithmeticException("Denominator should not be 0");
        }
        this.numerator = numerator;
        this.denominator = denominator;
        simplify();
    }

    private void simplify() {
        if (numerator < 0 && denominator < 0) {
            numerator = -numerator;
            denominator = -denominator;
        }
        int gcd = Utilities.greatestCommonFactor(numerator, denominator);
        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    public Fraction add(Fraction other) {
        return new Fraction(numerator * other.denominator + other.numerator * denominator, denominator * other.denominator);
    }

    public Fraction subtract(Fraction other) {
        return new Fraction(numerator * other.denominator - other.numerator * denominator, denominator * other.denominator);
    }

    public Fraction multiply(Fraction other) {
        return new Fraction(numerator * other.numerator, denominator * other.denominator);
    }

    public Fraction divide(Fraction other) {
        return new Fraction(numerator * other.denominator, denominator * other.numerator);
    }

    public double toDouble(){
        return 1.0*numerator / denominator;
    }

    public boolean isPositive(){
        return numerator >=0 && denominator >= 0;
    }

    public boolean isNegative(){
        return !isPositive();
    }
}
